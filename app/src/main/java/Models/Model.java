package Models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import Events.Detail;
import Events.Ready;
import Presenters.PresenterData;
import by.ivanessence.omertex.ActivityWithData;


public class Model {

    private static final String TAG = "Model";
    private static final String URL_1 = "https://jsonplaceholder.typicode.com/posts/";
    private static String URL_2 = "https://api.unsplash.com/photos/random?client_id=1f29173470cb51f497ddb2b9017eebe659e27d1b90c9ffca68a8f3ea2b25be31";
    private int quantity;
    private PresenterData presenterData;
    private boolean infoComplete;
    private boolean photosComplete;
    public static List<String> infoList;
    public static List<String> additionalInfoList;
    public static List<String> photosLinks;
    private DownloadPhoto downloadPhoto;

    public void getData(int quantity) {
        this.quantity = quantity;
        if (quantity > 1) {
            URL_2 = URL_2 + "&count=" + String.valueOf(quantity);
        }
        infoComplete = false;
        photosComplete = false;
        new GetInfo().execute();
        new GetPhotos().execute();
    }

    public List getList() {
        return additionalInfoList;
    }

    private class GetInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(URL_1);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(6000);
                conn.setDoInput(true);
                conn.setDoOutput(false);
                conn.connect();
                InputStream inputStream;
                int rc = conn.getResponseCode();
                if (rc != HttpURLConnection.HTTP_OK) {
                    inputStream = conn.getErrorStream();
                } else {
                    inputStream = conn.getInputStream();
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder content = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();
                Log.i("response", content.toString());
                try {
                    infoList = new ArrayList<String>();
                    additionalInfoList = new ArrayList<String>();
                    JSONArray jsonArray = new JSONArray(content.toString());
                    if (quantity > 1) {
                        Log.i(TAG, "jsonArrayLength: " + jsonArray.length());
                        for (int i = 0; i < quantity; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String title = jsonObject.getString("id");
                            String body = jsonObject.getString("body");
                            Log.i(TAG, "doInBackground: " + title);
                            infoList.add(title);
                            additionalInfoList.add(body);
                        }
                    } else {
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String title = jsonObject.getString("id");
                        String body = jsonObject.getString("body");
                        Log.i(TAG, "doInBackground: " + title);
                        infoList.add(title);
                        additionalInfoList.add(body);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i(TAG, "onPostExecute: " + infoList.size());
            infoComplete = true;
            if (photosComplete) {
                EventBus.getDefault().post(new Ready("ready"));
            }

        }
    }

    private class GetPhotos extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(URL_2);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(6000);
                conn.setDoInput(true);
                conn.setDoOutput(false);
                conn.connect();
                InputStream inputStream;
                int rc = conn.getResponseCode();
                if (rc != HttpURLConnection.HTTP_OK) {
                    inputStream = conn.getErrorStream();
                } else {
                    inputStream = conn.getInputStream();
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder content = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();
                Log.i("response", content.toString());
                try {
                    photosLinks = new ArrayList<String>();
                    if (quantity > 1) { //отличие в том, что при quantity > 1 мы возвращаем array
                        JSONArray jsonArray = new JSONArray(content.toString());
                        Log.i(TAG, "jsonArrayLENGTH: " + jsonArray.length());
                        for (int i = 0; i < quantity; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            JSONObject jsonObject1 = (JSONObject) jsonObject.get("links");
                            String link = jsonObject1.getString("download");
                            photosLinks.add(link);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject(content.toString());
                        JSONObject jsonObject1 = (JSONObject) jsonObject.get("links");
                        String link = jsonObject1.getString("download");
                        photosLinks.add(link);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i(TAG, "onPostExecute: " + photosLinks.size());
            photosComplete = true;
            if (infoComplete) {
                if(photosLinks.size() ==0) {
                    EventBus.getDefault().post(new Ready("fail"));
                } else {
                    EventBus.getDefault().post(new Ready("ready"));
                }

            }

        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }

    public void downloadFromUrl(String url) {
        downloadPhoto = new DownloadPhoto();
        downloadPhoto.execute(url);
    }

    public void cancel() {
        downloadPhoto.cancel(true);
    }

    private class DownloadPhoto extends AsyncTask<String, Void, Void> {
        Bitmap bitmap;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            Log.i(TAG, "DownloadPhoto: " + params[0]);
            bitmap = getBitmapFromURL(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i(TAG, "onPostExecuteDownloadPhoto");
            EventBus.getDefault().post(new Detail(bitmap));
        }
    }

}