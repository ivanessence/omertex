package Presenters;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Models.Model;
import by.ivanessence.omertex.ActivityWithData;

/**
 * Created by Ivan on 25.10.2017.
 */
public class PresenterData {

    private static final String TAG = "PresenterData";
    private ActivityWithData view;
    private int quantity;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        Log.i(TAG, "setQuantity: " + quantity);
    }

    public void addView(ActivityWithData activityWithData) {
        view = activityWithData;
        Model model = new Model();
        model.getData(quantity);
    }


}