package Presenters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;

import by.ivanessence.omertex.ActivityWithData;
import by.ivanessence.omertex.MainActivity;
import by.ivanessence.omertex.R;

public class PresenterMain {

    private MainActivity view;

    public void addView(MainActivity mainActivity) {
        view = mainActivity;
    }

    public void showAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view);
        final LayoutInflater inflater = (view).getLayoutInflater();
        alertDialogBuilder.setTitle(R.string.mainDialogHello);
        alertDialogBuilder
                .setMessage(R.string.mainDialogMessage)
                .setCancelable(false)
                .setView(inflater.inflate(R.layout.main_dialog, null))
                .setPositiveButton(R.string.mainDialogNext, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editQuantity;
                        editQuantity = (EditText) ((AlertDialog) dialog).findViewById(R.id.editTextQuantity);
                        int quantity = 0;
                        try {
                            quantity = Integer.valueOf(editQuantity.getText().toString());
                        } catch (NumberFormatException e) {
                            showToast();
                            return;
                        }
                        if (quantity < 1 || quantity > 30) {
                            showToast();
                        } else {
                            Intent nextActivity = new Intent(view, ActivityWithData.class);
                            nextActivity.putExtra("quantity", quantity);
                            view.startActivity(nextActivity);
                        }
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void showToast() {
        Toast.makeText(view, R.string.incorrectQuantity, Toast.LENGTH_LONG).show();
    }
}
