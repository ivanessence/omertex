package Events;

/**
 * Created by ivanessence on 27.10.2017.
 */

public class Ready {

    private String ready;

    public Ready(String ready) {
        this.ready = ready;
    }

    public String getReady() {
        return ready;
    }
}
