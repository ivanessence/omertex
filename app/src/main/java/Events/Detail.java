package Events;

import android.graphics.Bitmap;

public class Detail {

    private Bitmap bitmap;

    public Detail(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

}
