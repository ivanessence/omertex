package by.ivanessence.omertex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;

import Presenters.PresenterMain;


public class MainActivity extends AppCompatActivity {

    private Button buttonBegin;
    private PresenterMain presenterMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus myEventBus = EventBus.getDefault();
        initialize();
    }

    private void initialize() {
        presenterMain = new PresenterMain();
        presenterMain.addView(this);
        buttonBegin = (Button) findViewById(R.id.buttonBegin);
        buttonBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenterMain.showAlert();
            }
        });
    }
}
