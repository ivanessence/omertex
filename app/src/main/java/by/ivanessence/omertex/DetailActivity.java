package by.ivanessence.omertex;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import Events.Detail;
import Models.Model;

public class DetailActivity extends AppCompatActivity {

    private static final String TAG = "DetailActivity";
    private static final int HANDLER_SHOW_PHOTO = 13;
    private ImageView imageView;
    private TextView textViewTitle;
    private TextView textViewBody;
    private ProgressBar progressBar;
    private Intent intent;
    private List additionalInfoList;
    private String textData;
    private Model model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        EventBus.getDefault().register(this);
        initialize();
    }

    private void initialize() {
        imageView = (ImageView) findViewById(R.id.imageDetail);
        progressBar = (ProgressBar) findViewById(R.id.progressBarDetail);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewBody = (TextView) findViewById(R.id.textViewBody);
        intent = getIntent();
        model = new Model();
        additionalInfoList = model.getList();
        model.downloadFromUrl(intent.getStringExtra("photos"));
        textData = intent.getStringExtra("text");
        Log.i(TAG, "onCreate: " + intent.getStringExtra("text"));
        Log.i(TAG, "onCreate: " + intent.getStringExtra("photos"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        model.cancel();
    }


    @Subscribe
    public void onMessageEvent(Detail detail) {
        Message msg = new Message();
        msg.what = HANDLER_SHOW_PHOTO;
        msg.obj = detail.getBitmap();
        handler.sendMessage(msg);
    }



    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLER_SHOW_PHOTO:
                    progressBar.setVisibility(View.INVISIBLE);
                    textViewTitle.setText(textData);
                    int position = Integer.valueOf(textData);
                    textViewBody.setText(String.valueOf(additionalInfoList.get(position - 1)));
                    imageView.setImageBitmap((Bitmap)msg.obj);
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };


}
