package by.ivanessence.omertex;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Text;

import java.util.ArrayList;

import Events.Ready;
import Models.Model;
import Presenters.PresenterData;


public class ActivityWithData extends AppCompatActivity {

    public static final String TAG = "ActivityWithData";
    private PresenterData presenterData;
    private ProgressBar progressBar;
    private TextView textViewData;
    private ListView listView;
    private CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_data);
        EventBus.getDefault().register(this);
        initialize();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initialize() {
        listView = (ListView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        textViewData = (TextView) findViewById(R.id.textViewData);
        Intent intent = getIntent();
        int quantity = intent.getIntExtra("quantity", 0);
        presenterData = new PresenterData();
        presenterData.setQuantity(quantity);// quantity of elements that user checked
        presenterData.addView(this);
        Log.i(TAG, "initialize: ");
    }

    public void createList() {
        Log.i(TAG, "createList: ");
        progressBar.setVisibility(View.INVISIBLE);
        customAdapter = new CustomAdapter(this, (ArrayList) Model.infoList, (ArrayList) Model.photosLinks);
        listView.setAdapter(customAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Ready event) {
        if (event.getReady().equals("ready")) {
            createList();
        } else {
            textViewData.setText(R.string.textFail);
            progressBar.setVisibility(View.INVISIBLE);
        }

    }
}
