package by.ivanessence.omertex;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by ivanessence on 27.10.2017.
 */

public class CustomAdapter extends BaseAdapter{

    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<String> text;
    private ArrayList<String> photos;

    public CustomAdapter(Context context, ArrayList text, ArrayList photos) {
        this.ctx = context;
        this.text = text;
        this.photos = photos;
        inflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return text.size();
    }

    @Override
    public Object getItem(int position) {
        text.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter, parent, false);
        }
        ((TextView) view.findViewById(R.id.info)).setText(text.get(position));
        ((TextView) view.findViewById(R.id.url)).setText(photos.get(position));
        view.findViewById(R.id.image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ctx, text.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ctx, DetailActivity.class);
                intent.putExtra("text",text.get(position));
                intent.putExtra("photos",photos.get(position));
                ctx.startActivity(intent);
            }
        });
        return view;
    }
}
